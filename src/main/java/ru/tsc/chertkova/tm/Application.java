package ru.tsc.chertkova.tm;

import ru.tsc.chertkova.tm.constant.ArgumentConst;
import ru.tsc.chertkova.tm.constant.TerminalConst;

import java.util.Scanner;

public final class Application {

    public static void main(final String[] args) {
        displayWelcome();
        run(args);
        process();
    }

    private static void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            System.out.println("ENTER COMMAND:");
            command = scanner.nextLine();
            run(command);
            System.out.println();
        }
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
        System.out.println("exit - Terminate console application.");
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.3");
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Yuliya Chertkova");
        System.out.println("ychertkova@t1-consulting.com");
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void exit() {
        System.exit(0);
    }

    private static void run(final String[] args) {
        if (args == null || args.length < 1) return;
        final String param = args[0];
        if (param == null || param.isEmpty()) return;
        switch (param) {
            case ArgumentConst.CMD_HELP:
                displayHelp();
                break;
            case ArgumentConst.CMD_VERSION:
                displayVersion();
                break;
            case ArgumentConst.CMD_ABOUT:
                displayAbout();
                break;
            default:
                showError(param);
        }
    }

    private static void run(final String param) {
        if (param == null || param.isEmpty()) return;
        switch (param) {
            case TerminalConst.CMD_HELP:
                displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                displayAbout();
                break;
            case TerminalConst.EXIT:
                exit();
            default:
                showError(param);
        }
    }

    private static void showError(final String param) {
        System.out.printf("Error! This argument '%s' not supported...\n", param);
    }

}
